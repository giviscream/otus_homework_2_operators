﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fractions
{
    //Here is the Fraction class of mine
    //Unfortunately, many things are not realized or are simplified, cause I want to goal another tasks and don't have much time for it :)
    class Fraction
    {
        //As mentioned before I simplified setting of numerator and denominator. Just set values with constructor.
        private Int32 numerator;
        public Int32 Numerator { get => numerator; }

        private Int32 denominator;
        public Int32 Denominator { get => denominator; }

        public Fraction(int numerator, int denominator)
        {
            
            this.numerator = ((denominator > 0) ? 1 : -1) * numerator;
            this.denominator = Math.Abs(denominator);
        }
        //It's reduced method. For example, it reduces 2/4 to 1/2
        public Fraction GetReduced()
        {
            int gsd = Reducing.Gsd(numerator, denominator);

            return new Fraction(numerator / gsd, denominator / gsd);
        }

        public override string ToString()
        {
            return $"{this.numerator} / {this.denominator}";
        }
        #region Operators Overloading
        //Just overloading of operators
        public static Fraction operator*(Fraction fraction1, int num)
        {
            return new Fraction(fraction1.numerator * num, fraction1.denominator * num);
        }

        public static Fraction operator*(Fraction fraction1, Fraction fraction2)
        {
            return new Fraction(fraction1.numerator * fraction2.numerator, fraction1.denominator * fraction2.denominator);
        }

        public static Fraction operator/(Fraction fraction1, int num)
        {
            return new Fraction(fraction1.numerator, fraction1.denominator * num);
        }

        public static Fraction operator/(Fraction fraction1, Fraction fraction2)
        {
            return new Fraction(fraction1.numerator * fraction2.denominator, fraction2.denominator * fraction1.denominator);
        }

        public static Fraction operator+ (Fraction fraction1, Fraction fraction2)
        {
            int gsd = Reducing.Gsd(fraction1.denominator, fraction2.denominator);

            int coeff1 = fraction2.denominator / gsd;
            int coeff2 = fraction1.denominator / gsd;

            int newNumerator = fraction1.numerator * coeff1 + fraction2.numerator * coeff2;
            int newDenominator = fraction1.denominator * coeff1;

            return new Fraction(newNumerator, newDenominator);
        }
        #endregion Operators Overloading

    }
    //It's for indexator
    class Fractions
    {
        Fraction[] fractions;

        public Fractions()
        {
            fractions = new Fraction[0];
        }

        public Fraction this[int index]
        {
            get
            {
                return fractions[index];
            }

            set
            {
                if (index >= 0 && index >= fractions.Length)
                {
                    Array.Resize<Fraction>(ref fractions, index + 1);
                }

                fractions[index] = value;
            }
        }
    }


    
}
