﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Linq;

namespace Fractions
{
    //This is the extendor of strings for beatiful printing of fraction expressions.
    static class StringFractionExtender
    {
        public static String SetOutFraction(this String inStr)
        {
            Regex regReplSpaces = new Regex(@"\s");
            inStr = regReplSpaces.Replace(inStr, "");

            Regex regDigit = new Regex(@"\d{1,}");
            Regex regSign = new Regex(@"\W");

            String[] digits = regSign.Split(inStr);

            String[] symbols = regDigit.Split(inStr).Where(x => x != "").ToArray<String>();

            String lowPart = "";
            String middlePart = "";
            String highPart = "";

            for (int i = 0; i < digits.Length; i+= 2)
            {
                if (lowPart != "")
                    lowPart += "  ";

                if (middlePart != "")
                    middlePart += " ";

                if (highPart != "")
                    highPart += "   ";

                middlePart += string.Concat(Enumerable.Repeat("-", digits[i + 1].Length)) 
                                + ((symbols.Length > i + 1) ? " " + symbols[i + 1] : "");

                int numOfSpacesBefore = (digits[i + 1].Length - digits[i].Length) / 2;
                int numOfSpacesAfter = digits[i + 1].Length - digits[i].Length - numOfSpacesBefore;

                highPart += string.Concat(Enumerable.Repeat(" ", numOfSpacesBefore)) + digits[i] + string.Concat(Enumerable.Repeat(" ", numOfSpacesAfter));

                lowPart +=  digits[i + 1] + " ";


            }


            return highPart + "\n" + middlePart + "\n" + lowPart;
        }
    }
}
