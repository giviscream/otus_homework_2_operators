﻿using System;
using System.Linq;

namespace Fractions
{
    class Program
    {
        //This is a testing(demonstrting) part of my homework number 2.
        //My idea for this task is creating class which can handle with frctions.
        //I create 2 fractions for demonstration and show some work with them.
        //Actually, I can't remmember how many hours I've lost with this task. Let it be 8.

        static void Main(string[] args)
        {
            //Create the first fraction
            Fraction fraction1 = new Fraction(4, 1000);
            Console.WriteLine($"Fraction1: {fraction1}\n");
            //Ok, let's reduce it.
            Console.WriteLine($"Reduced Fraction1: { fraction1.GetReduced()}\n");

            //Create the second fraction
            Fraction fraction2 = new Fraction(11, 50);
            Console.WriteLine($"Fraction2: {fraction2}\n");

            //Let's calc sum of them and print(Printing is my favourite part). It's made with extention of String class
            Fraction sumFraction = fraction1 + fraction2;
            Console.WriteLine((fraction1.ToString() + " + " + fraction2.ToString() + " = " + sumFraction.ToString()).SetOutFraction() + "\n");
            //Division of 2 fractions
            Fraction divFraction = fraction1 / fraction2;
            Console.WriteLine((fraction1.ToString() + " : " + fraction2.ToString() + " = " + divFraction.ToString()).SetOutFraction() + "\n");
            //Multiplying of 2 fractions
            Fraction multyFraction = fraction1 * fraction2;
            Console.WriteLine((fraction1.ToString() + " * " + fraction2.ToString() + " = " + multyFraction.ToString()).SetOutFraction() + "\n");
            //And indexator
            Fractions fractions = new Fractions();
            fractions[0] = new Fraction(1, 3);
            fractions[100] = new Fraction(35, 4000);

            Console.WriteLine(fractions[100].ToString().SetOutFraction());



        }
    }
}
