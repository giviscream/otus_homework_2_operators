﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fractions
{
    //This the class for reducing fractions by greatest same delimeter
    public static class Reducing
    {
        public static int Gsd(int num1, int num2)
        {
            while (num1 != num2)
            {
                if (num1 < num2)
                {
                    num2 = num2 - num1;
                }
                else
                {
                    num1 = num1 - num2;
                }
            }

            return num1;
        }
    }
}
